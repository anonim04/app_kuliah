<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswerGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answer_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('respond_id')->unsigned();
            $table->text('answer')->nullable();
            $table->string('picAnswer')->nullable();
            $table->string('dirPic')->nullable();
            $table->string('type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answer_groups');
    }
}
