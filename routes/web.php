<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::middleware('auth')->group( function(){
    Route::get('/', 'HomeController@index');
    Route::get('/home', 'MaterialController@index')->name('home');
    Route::get('/material/download/modul/{id}','MaterialController@getFile');
    Route::get('/material/download/ppt/{id}','MaterialController@getFile');
    Route::get('/quiz/{id}','QuizController@index')->name('quiz.index');
    Route::get('/quiz/show/{quiz_id}/individu','QuizController@showIndividu ')->name('quiz.show.individu');
    Route::get('/quiz/show/{quiz_id}/group','QuizController@showGroup')->name('quiz.show.group')/*->middleware('IsRespond')*/;
    Route::post('/respond/save','RespondController@store')->name('respond.save');
    Route::middleware('admin')->group( function(){
        Route::post('/material/save','MaterialController@storeFile');
        Route::delete('/material/delete/{id}', 'MaterialController@destroy');
        Route::post('/quiz/save','QuizController@store');
        Route::delete('/quiz/delete/{id}', 'QuizController@destroy');
        Route::get('/quiz/{material_id}/group/create','GroupController@create')->name('group.create');
        Route::post('/quiz/{material_id}/group/save','GroupController@store')->name('group.save');
        Route::get('/respond/individu/{quiz_id}', 'RespondController@index')->name('respond.index.individu');
        Route::get('/respond/group/{quiz_id}', 'RespondController@group')->name('respond.index.group');
        Route::get('/respond/show/{respond_id}','RespondController@show')->name('respond.show');
        Route::get('/users','UserController@index')->name('user.index');
        Route::post('/user/save','UserController@store');
    });
});

use App\User;

Route::get('admin', function(){
    $user = User::create([
        'nim'=>'H13116303',
        'name'=>'Akbar',
        'level'=>'admin',
        'email'=>'test@gmail.com',
        'password'=>bcrypt('anonim1998'),
    ]);
    return $user;
});
