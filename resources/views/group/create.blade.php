@extends('layouts.app')

@section('content')
<div class="card is-fullwidth is-striped">
    <header class="card-header">
        <p class="card-header-title">
            PEMBAGIAN KELOMPOK
        </p>
    </header>
    <div class="card-content">
        <div class="content">
            {!! Form::open(['route'=>['group.save',$material_id], 'method' => 'post']) !!}
            <table class="table is-fullwidth is-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>NIM</th>
                         <th>Nama</th>
                        <th>Kelompok</th>
                        <th>Level</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no= ($pagination->currentpage() - 1) * $pagination->perpage() + 1;
                    @endphp
                    @foreach($users as $key => $user)
                        <tr>
                            <th>{{$no++}}</th>
                            <td>{{$user->nim}}</td>
                            {!! Form::hidden('user_id['.$key. ']', $user->id) !!}
                            <td>{{$user->name}}</td>
                            <td>
                                <div class="select">
                                    <select name="nameGroup[{{$key}}]">
                                        <option value="kelompok 1">kelompok 1</option>
                                        <option value="kelompok 2">kelompok 2</option>
                                        <option value="kelompok 3">kelompok 3</option>
                                        <option value="kelompok 4">kelompok 4</option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <div class="select">
                                    <select name="level[{{$key}}]">
                                        <option value="0">Anggota</option>
                                        <option value="1">Ketua</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="buttons is-right">
                {{Form::submit('Upload',['class'=>'button is-success is-right',])}}
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>
@endsection
