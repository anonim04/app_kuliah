@extends('layouts.templateAuth')

@section('content')
<form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
    @csrf

    <div class="field">
      <label class="label" for="email">{{ __('E-Mail Address') }}</label>
      <div class="control has-icons-right">
          <input id="email" type="email" class="{{ $errors->has('email') ? 'input is-danger':'input'}}" name="email" value="{{ old('email') }}" required>

          @if ($errors->has('email'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('email') }}</strong>
              </span>
          @endif
        <span class="icon is-small is-right">
          <i class="fa fa-envelope"></i>
        </span>
      </div>
    </div>

    <div class="has-text-centered">
        <button type="submit" class="button is-vcentered is-primary is-outlined">
            {{ __('Send Password Reset Link') }}
        </button>
    </div>
</form>
@endsection
