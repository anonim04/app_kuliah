@extends('layouts.templateAuth')

@section('content')
<div class="columns is-vcentered">
  <div class="login column is-4 ">
    <section class="section">
      <div class="has-text-centered">
          <img class="login-logo" src="{{ asset('img/Logo_Unhas.jpg') }}">
      </div>
      <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
          @csrf
          <div class="field">
            <label class="label" for="nim">{{ __('NIM') }}</label>
            <div class="control has-icons-right">
                <input id="nim" type="text" class="{{ $errors->has('nim') ? 'input is-danger':'input' }}" name="nim" value="{{ old('nim') }}" required autofocus>
                @if ($errors->has('nim'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('nim') }}</strong>
                    </span>
                @endif
              <span class="icon is-small is-right">
                <i class="fa fa-user"></i>
              </span>
            </div>
          </div>

          <div class="field">
            <label class="label" for="name">{{ __('Name') }}</label>
            <div class="control has-icons-right">
                <input id="name" type="text" class="{{ $errors->has('name') ? 'input is-danger':'input' }}" name="name" value="{{ old('name') }}" required autofocus>
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
              <span class="icon is-small is-right">
                <i class="fa fa-user"></i>
              </span>
            </div>
          </div>

          <div class="field">
            <label class="label" for="email">{{ __('E-Mail Address') }}</label>
            <div class="control has-icons-right">
                <input id="email" type="email" class="{{ $errors->has('email') ? 'input is-danger':'input'}}" name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              <span class="icon is-small is-right">
                <i class="fa fa-envelope"></i>
              </span>
            </div>
          </div>

          <div class="field">
              <div class="control">
                  <label class="label" for="level">{{ __('level') }}</label>
                  <label class="radio">
                      <input type="radio" name="level" value="admin">
                      Admin
                  </label>
                  <label class="radio">
                      <input type="radio" name="level" value="guest">
                      Guest
                  </label>
              </div>
              @if ($errors->has('level'))
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('level') }}</strong>
                  </span>
              @endif
          </div>

          <div class="field">
            <label class="label" for="password">{{ __('Password') }}</label>
            <div class="control has-icons-right">
                <input id="password" type="password" class="{{ $errors->has('password') ? 'input is-danger':'input' }}" name="password" required>

                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
              <span class="icon is-small is-right">
                <i class="fa fa-key"></i>
              </span>
            </div>
          </div>

          <div class="field">
            <label class="label" for="password-confirm">{{ __('Confirm Password') }}</label>
            <div class="control has-icons-right">
                <input id="password-confirm" type="password" class="{{ $errors->has('password') ? 'input is-danger':'input' }}" name="password_confirmation" required>

                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
              <span class="icon is-small is-right">
                <i class="fa fa-key"></i>
              </span>
            </div>
          </div>

          <div class="has-text-centered">
              <button type="submit" class="button is-vcentered is-primary is-outlined">
                  {{ __('Register') }}
              </button>
          </div>
          <div class="has-text-centered">
            <a href="{{ route('login') }}"> Already have an account? Log in now !</a>
          </div>
      </form>
    </section>
  </div>
  <div id="particles-js" class="interactive-bg column is-8"></div>
</div>

@endsection
