@extends('layouts.templateAuth')

@section('content')
<form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
    @csrf
    <div class="field">
        <label class="label" for="nim">{{ __('Nomor Induk Mahasiswa') }}</label>
        <div class="control has-icons-right">
            <input id="nim" type="input" class="{{ $errors->has('nim') ? 'input is-danger':'input' }}" name="nim" value="{{ old('email') }}" required autofocus>
            @if ($errors->has('nim'))
            <span class="help is-danger">
                <strong>{{ $errors->first('nim') }}</strong>
            </span>
            @endif
            <span class="icon is-small is-right">
                <i class="fa fa-user"></i>
            </span>
        </div>
    </div>


    <div class="field">
        <label class="label" for="password">{{ __('Password') }}</label>
        <div class="control has-icons-right">
            <input id="password" type="password" class="{{ $errors->has('password') ? 'input is-danger':'input' }}" name="password" required>

            @if ($errors->has('password'))
            <span class="help is-danger" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
            <span class="icon is-small is-right">
                <i class="fa fa-key"></i>
            </span>
        </div>
    </div>

    <div class="form-check">
        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

        <label class="form-check-label" for="remember">
            {{ __('Remember Me') }}
        </label>
    </div>

    <div class="has-text-centered">
        <button type="submit" class="button is-vcentered is-primary is-outlined">{{ __('Login') }}</button>
    </div>
</form>
<div class="has-text-centered">
  <a href="{{ route('password.request') }}"> {{ __('Forgot Your Password?') }}</a>
</div>
@stop
