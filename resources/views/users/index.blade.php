@extends('layouts.app')

@section('content')
<div class="card is-fullwidth is-striped">
    <header class="card-header">
        <p class="card-header-title">
            DATA USER
        </p>
        <span class="card-header-icon" aria-label="more options" data-target="modal-id">
            <button class="button modal-button is-success" data-toggle="modal"  data-target="modalCreateUser">
                <span class="icon">
                  <i class="fas fa-plus" aria-hidden="true"></i>
                </span>
            </button>
        </span>
        <div id="modalCreateUser" class="modal modal-fx-3dSlit">
            <div class="modal-background"></div>
                <div class="modal-content">
                    <header class="modal-card-head">
                        <div class="field">
                            <p class="modal-card-title">
                                Tambah User
                            </p>
                        </div>
                    </header>
                    <section class="modal-card-body">
                        {{ Form::open(['url'=>'/user/save', 'method' => 'post']) }}
                            @csrf
                            <div class="field">
                              <label class="label" for="nim">{{ __('NIM') }}</label>
                              <div class="control has-icons-right">
                                  <input id="nim" type="text" class="{{ $errors->has('nim') ? 'input is-danger':'input' }}" name="nim" value="{{ old('nim') }}" required autofocus>
                                  @if ($errors->has('nim'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('nim') }}</strong>
                                      </span>
                                  @endif
                                <span class="icon is-small is-right">
                                  <i class="fa fa-user"></i>
                                </span>
                              </div>
                            </div>

                            <div class="field">
                              <label class="label" for="name">{{ __('Name') }}</label>
                              <div class="control has-icons-right">
                                  <input id="name" type="text" class="{{ $errors->has('name') ? 'input is-danger':'input' }}" name="name" value="{{ old('name') }}" required autofocus>
                                  @if ($errors->has('name'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('name') }}</strong>
                                      </span>
                                  @endif
                                <span class="icon is-small is-right">
                                  <i class="fa fa-user"></i>
                                </span>
                              </div>
                            </div>

                            <div class="field">
                              <label class="label" for="email">{{ __('E-Mail Address') }}</label>
                              <div class="control has-icons-right">
                                  <input id="email" type="email" class="{{ $errors->has('email') ? 'input is-danger':'input'}}" name="email" value="{{ old('email') }}" required>

                                  @if ($errors->has('email'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('email') }}</strong>
                                      </span>
                                  @endif
                                <span class="icon is-small is-right">
                                  <i class="fa fa-envelope"></i>
                                </span>
                              </div>
                            </div>

                            <div class="field">
                                <div class="control">
                                    <label class="label" for="level">{{ __('level') }}</label>
                                    <label class="radio">
                                        <input type="radio" name="level" value="admin">
                                        Admin
                                    </label>
                                    <label class="radio">
                                        <input type="radio" name="level" value="guest">
                                        Guest
                                    </label>
                                </div>
                                @if ($errors->has('level'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('level') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="field">
                              <label class="label" for="password">{{ __('Password') }}</label>
                              <div class="control has-icons-right">
                                  <input id="password" type="password" class="{{ $errors->has('password') ? 'input is-danger':'input' }}" name="password" required>

                                  @if ($errors->has('password'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('password') }}</strong>
                                      </span>
                                  @endif
                                <span class="icon is-small is-right">
                                  <i class="fa fa-key"></i>
                                </span>
                              </div>
                            </div>

                            <div class="field">
                              <label class="label" for="password-confirm">{{ __('Confirm Password') }}</label>
                              <div class="control has-icons-right">
                                  <input id="password-confirm" type="password" class="{{ $errors->has('password') ? 'input is-danger':'input' }}" name="password_confirmation" required>

                                  @if ($errors->has('password'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('password') }}</strong>
                                      </span>
                                  @endif
                                <span class="icon is-small is-right">
                                  <i class="fa fa-key"></i>
                                </span>
                              </div>
                            </div>

                            <div class="has-text-right">
                                <button type="submit" class="button is-vright is-primary is-outlined">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </form>
                    </section>
                </div>
            <button class="modal-close is-large" aria-label="close"></button>
        </div>

    </header>
    <div class="card-content">
        <div class="content">
            <table class="table is-fullwidth is-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>NIM</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Level</th>
                        <th>Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no= ($pagination->currentpage() - 1) * $pagination->perpage() + 1;
                    @endphp
                    @foreach($users as $user)
                        <tr>
                            <th>{{$no++}}</th>
                            <td>{{$user->nim}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->level}}</td>
                            <td>
                                <button class="button modal-button is-danger" data-toggle="modal"  data-target="modalDeleteFile{{$user->id}}">
                                    <span class="icon">
                                      <i class="fas fa-trash-alt" aria-hidden="true"></i>
                                    </span>
                                </button>
                                <div id="modalDeleteFile{{$user->id}}" class="modal modal-fx-3dSlit">
                                    <div class="modal-background"></div>
                                    <div class="modal-content">
                                        <header class="modal-card-head">
                                            <div class="field">
                                                <p class="modal-card-title">
                                                    Konfirmasi Hapus Data
                                                </p>
                                            </div>
                                        </header>
                                        <section class="modal-card-body">
                                            Apakah Anda Yakin ingin Menghapus Data Ini ?
                                        </section>
                                        <footer class="modal-card-foot buttons is-right">
                                            <button type="button" name="button" class="button">Cancel</button>
                                            {{ Form::open(['url'=>'/user/delete/'.$user->id, 'method' => 'delete']) }}
                                            <button type="submit" class="button is-danger">
                                                Konfirmasi
                                            </button>
                                            {{ Form::close() }}
                                        </footer>
                                    </div>
                                    <button class="modal-close is-large" aria-label="close"></button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $pagination->links('vendor.pagination.bulma') }}
        </div>
    </div>
</div>
@endsection
