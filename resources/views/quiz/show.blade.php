@extends('layouts.app')

@section('content')
<div class="steps" id="stepsDemo">
    <div class="step-item is-active is-success"></div>
    <div class="step-item"></div>
    <div class="step-item"></div>
    <div class="step-item"></div>
    <div class="steps-content">
        @php
            $number = 1;
        @endphp
        <div class="label has-text-centered">Fill in the answer closes in <span id="time">10:00</span> minutes!</div>
        {{ Form::open(['url'=>'/respond/save', 'method' => 'post','files'=>true,'enctype' => 'multipart/form-data','id'=>'test']) }}
        @foreach($question as $key => $value)
            {!! Form::hidden('user_id',Auth::user()->id) !!}
            {!! Form::hidden('quiz_id',$quiz_id) !!}
            <div class="step-content {{$key%3==0 ? 'is-active':''}}">
                <div class="field">
                    {!! Form::label('answer',$number.'. '.$question[$key]->question,['class'=>'label']) !!}
                    <div class="field-body">
                        <div class="field">
                            <div class="control">
                                <textarea class="summernote"name="answer[{{$number}}]" placeholder="Jawaban" autofocus data-validate="require" ></textarea>
                                <div class="file has-name  is-info">
                                    <label class="file-label">
                                        <input type="file" name="picAnswer[{{$number++}}]" class="file-input" roles="form" multiple="multiple">
                                        <span class="file-cta">
                                            <span class="file-icon">
                                                <i class="fas fa-upload"></i>
                                            </span>
                                            <span class="file-label">
                                                Upload Picture…
                                            </span>
                                        </span>
                                        <span class="file-name">
                                            -
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
            <div class="step-content has-text-centered">
                <h1 class="title is-4">You are great!</h1>
                <h2 class="title is-4">press save when finished!</h2>
                <div class="buttons is-centered">
                    <a href="{{url('/home')}}" class="button">Cancel</a>
                    {!! Form::submit('Simpan',['class'=>'button is-success',]) !!}
                </div>
            </div>
            {{Form::close()}}
        <div class="steps-actions">
            <div class="steps-action">
                <a data-nav="previous" class="button is-light">Previous</a>
            </div>
            <div class="steps-action">
                <a data-nav="next" class="button is-light">Next</a>
            </div>
        </div>
    </div>
</div>
<style>
    body { counter-reset: number;}
    p::before {
    	counter-increment: number;
        content: counter(number)". ";
        padding-right: 8px;
        padding-left: 16px;
      	list-style: none; /* Remove HTML bullets */
     	padding: 0;
      	margin: 0;
        color: #C0C0C0;
    }
</style>
@endsection
