@extends('layouts.app')

@section('content')
    <div class="card">
        <header class="card-header">
            <p class="card-header-title">
                Quiz
            </p>
            @endif
        </header>
        <div class="card-content">
            <div class="content">
                {!! Form::open(['url'=>'/quiz/save', 'method' => 'post']) !!}
                {!! Form::hidden('material_id',$material_id) !!}
                <div class="field is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">Title</label>
                    </div>
                    <div class="field-body">
                        {!! Form::text('title','',['class'=>'input']) !!}
                    </div>
                </div>

                <div class="field is-horizontal">
                    <div class="field-label">
                        <label class="label">Pengerjaan</label>
                    </div>
                    <div class="field-body">
                        <div class="select">
                            <select name="answer">
                                <option value="kelompok">kelompok</option>
                                <option value="individu">individu</option>
                            </select>
                        </div>
                    </div>
                </div>

                @for($index = 0; $index<'3'; $index++)
                    <div class="field">
                        {{Form::label('pertanyaan','Pertanyaan '.$index,['class'=>'label'])}}
                        <div class="control">
                            <textarea class="summernote"name="question[{{$index}}]" placeholder="Pertenyaan" autofocus data-validate="require"  ></textarea>
                        </div>
                    </div>
                @endfor
                <button type="button" class="button button-close-modal" aria-label="close">
                    Cancel
                </button>
                {{Form::submit('Upload',['class'=>'button is-success is-right',])}}
                {{Form::close()}}
            </div>
        </div>
    </div>
@endsection
