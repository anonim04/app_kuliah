@extends('layouts.app')

@section('content')
<div class="card">
    <header class="card-header">
        <span class="card-header-icon" aria-label="more options" data-target="modal-id">
            <a href="{{ url()->previous() }}"  aria-label="more options">
                <span class="icon">
                    <i class="fas fa-arrow-circle-left" aria-hidden="true"></i>
                </span>
            </a>
        </span>
        <p class="card-header-title">
            Kelompok
        </p>
        @if(Auth::user()->level=='admin')
        <span class="card-header-icon" aria-label="more options" data-target="modal-id">
            <a href="{{route('group.create',$material_id)}}"  aria-label="more options">
                <span class="icon">
                    <i class="fas fa-plus" aria-hidden="true"></i>
                </span>
            </a>
        </span>
        @endif
    </header>
    <div class="card-content">
        <div class="content">
                <div class="columns">
                    @for($repeat = 1; $repeat<='4'; $repeat++)
                      <div class="column">
                          <table class="table is-fullwidth is-striped">
                              <thead>
                                  <th colspan="2">
                                      KELOMPOK {{$repeat}}
                                  </th>
                              </thead>
                              <tbody>
                                  @foreach($group as $key=>$value)
                                  @if($value->nameGroup=='kelompok '.$repeat)
                                  <tr>
                                      <td>
                                          {{$value->user->name}}
                                      </td>
                                      <td>
                                          {{$value->user->nim}}
                                          @if ($value->leader=='1')
                                              <b>~</b>
                                          @endif
                                      </td>
                                  </tr>
                                  @endif
                                  @endforeach
                              </tbody>
                          </table>
                      </div>
                  @endfor
                </div>
        </div>
    </div>
</div>

<br>
<br>

<div class="card">
    <header class="card-header">
        <p class="card-header-title">
            Quiz
        </p>
        @if(Auth::user()->level=='admin')
        <span class="card-header-icon" aria-label="more options" data-target="modal-id">
            <button class="button modal-button is-success" data-toggle="modal"  data-target="modalInsertQuiz">
                <span class="icon">
                    <i class="fas fa-pencil-alt" aria-hidden="true"></i>
                </span>
            </button>
            <div id="modalInsertQuiz" class="modal modal-fx-3dSlit">
                <div class="modal-background"></div>
                <div class="modal-content">
                    <header class="modal-card-head">
                        <div class="field">
                            <p class="modal-card-title">
                                Tambah Soal Quiz
                            </p>
                        </div>
                    </header>
                    @php
                        $no= ($question->currentpage() - 1) * $question->perpage() + 1;
                    @endphp
                    <section class="modal-card-body">
                        {{ Form::open(['url'=>'/quiz/save', 'method' => 'post']) }}
                        {!! Form::hidden('material_id',$material_id) !!}
                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label">Title</label>
                            </div>
                            <div class="field-body">
                                {!! Form::text('title','',['class'=>'input']) !!}
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                <label class="label">Pengerjaan</label>
                            </div>
                            <div class="field-body">
                                <div class="select">
                                    <select name="answer">
                                        <option value="kelompok">kelompok</option>
                                        <option value="individu">individu</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        @for($index = 0; $index<'3'; $index++)
                            <div class="field">
                                {{Form::label('pertanyaan','Pertanyaan '.$index,['class'=>'label'])}}
                                <div class="control">
                                    <textarea class="summernote"name="question[{{$index}}]" placeholder="Pertenyaan" autofocus data-validate="require"  ></textarea>
                                </div>
                            </div>
                        @endfor
                    </section>
                    <footer class="modal-card-foot buttons is-right">
                        <button type="button" class="button button-close-modal" aria-label="close">
                            Cancel
                        </button>
                        {{Form::submit('Upload',['class'=>'button is-success is-right',])}}
                    </footer>
                    {{Form::close()}}
                </div>
                <button class="modal-close is-large" aria-label="close"></button>
            </div>
        </span>
        @endif
    </header>
    <div class="card-content">
        <div class="content">
            <table class="table is-fullwidth is-striped">
                <tbody>
                    @php
                        $leader = \App\GroupStudent::where('user_id', Auth::user()->first()->id)
                    @endphp
                    @foreach($question as $key=>$value)

                    <tr>
                        <th>{{$no++}}</th>
                        <th>
                            {{$value->title}}
                        </th>
                        <td>
                            <div class="buttons is-right">
                                @if($value->answer=='kelompok' && $leader)
                                <a href="{{route('quiz.show.group', $value->id)}}" class="button is-info" >
                                    <span class="icon">
                                        <i class="fas fa-file-signature" aria-hidden="true"></i>
                                    </span>
                                </a>
                                @elseif ($value->answer=='individu')
                                    <a href="{{route('quiz.show.individu', $value->id)}}" class="button is-info" >
                                        <span class="icon">
                                            <i class="fas fa-file-signature" aria-hidden="true"></i>
                                        </span>
                                    </a>
                                @endif
                            </div>
                        </td>
                        @if(Auth::user()->level=='admin')
                        <td>
                            @if($value->answer=='kelompok')
                                <div class="buttons is-centered">
                                    <a href="{{route('respond.index.group', $value->id)}}" class="button is-warning">
                                        <span class="icon">
                                            <i class="fas fa-address-book" aria-hidden="true"></i>
                                        </span>
                                    </a>
                                </div>
                            @elseif ($value->answer=='individu')
                                <div class="buttons is-centered">
                                    <a href="{{route('respond.index.individu', $value->id)}}" class="button is-warning">
                                        <span class="icon">
                                            <i class="fas fa-address-book" aria-hidden="true"></i>
                                        </span>
                                    </a>
                                </div>
                            @endif

                        </td>
                        <td>
                            <div class="buttons">
                                <button class="button modal-button is-danger" data-toggle="modal"  data-target="modalDeleteQuiz{{$value->id}}">
                                    <span class="icon">
                                        <i class="fas fa-trash-alt" aria-hidden="true"></i>
                                    </span>
                                </button>
                            </div>
                            <div id="modalDeleteQuiz{{$value->id}}" class="modal modal-fx-3dSlit">
                                <div class="modal-background"></div>
                                <div class="modal-content">
                                    <header class="modal-card-head">
                                        <div class="field">
                                            <p class="modal-card-title">
                                                Konfirmasi Hapus Data
                                            </p>
                                        </div>
                                    </header>
                                    <section class="modal-card-body">
                                        Apakah Anda Yakin ingin Menghapus Data Ini ?
                                    </section>
                                    <footer class="modal-card-foot buttons is-right">
                                        <button type="button" name="button" class="button">Cancel</button>
                                        {{ Form::open(['url'=>'/quiz/delete/'.$question[$key]->id, 'method' => 'delete']) }}
                                        <button type="submit" class="button is-danger">
                                            Konfirmasi
                                        </button>
                                        {{ Form::close() }}
                                    </footer>
                                </div>
                                <button class="modal-close is-large" aria-label="close"></button>
                            </div>
                        </td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $question->links('vendor.pagination.bulma') }}
        </div>
    </div>
</div>
@endsection
