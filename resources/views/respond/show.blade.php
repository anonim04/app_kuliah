@extends('layouts.app')

@section('content')
<div class="card is-fullwidth is-striped">
    <header class="card-header">
        <a href="{{ url()->previous() }}" class="card-header-icon" aria-label="more options">
            <span class="icon">
                <i class="fas fa-arrow-circle-left" aria-hidden="true"></i>
            </span>
        </a>
        <p class="card-header-title">
            Respond
        </p>
    </header>
    <div class="card-content">
        <div class="content">
            <table class="table is-fullwidth is-striped">
                <tbody>
                    <tr>
                        <th>NIM</th>
                        <td>{{$respond->user->nim}}</td>
                    </tr>
                    <tr>
                        <th>Nama</th>
                        <td>{{$respond->user->name}}</td>
                    </tr>
                </tbody>
            </table>
            <table>
                <tbody>
                    @php
                        $no = 1;
                        $index = 0;
                    @endphp
                    @foreach($question as $key =>$value)
                        <tr>
                            <th>{{$no++}}. {{$question[$key]->question}}</th>
                        </tr>
                        @if($answer[$index]->answer)
                            <tr>
                                <td> {!! $answer[$index]->answer !!}</td>
                            </tr>
                        @endif
                        @if($answer[$index]->picAnswer)
                            <tr>
                                <td> <img src="{{asset('../storage/app/picture/answer/'.$answer[$key]->picAnswer)}}" ></td>
                            </tr>
                        @endif
                        @php
                            $index++;
                        @endphp
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
