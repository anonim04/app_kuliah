@extends('layouts.app')

@section('content')
<div class="card is-fullwidth is-striped">
    <header class="card-header">
        <span class="card-header-icon" aria-label="more options" data-target="modal-id">
            <a href="{{ url()->previous() }}"  aria-label="more options">
                <span class="icon">
                    <i class="fas fa-arrow-circle-left" aria-hidden="true"></i>
                </span>
            </a>
        </span>
        <p class="card-header-title">
            DATA RESPOND
        </p>
    </header>
    <div class="card-content">
        <div class="content">
            <table class="table is-fullwidth is-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>NIM</th>
                        <th>NAMA</th>
                        <th>Respond</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                      $i= ($responds->currentpage() - 1) * $responds->perpage() + 1;
                    @endphp
                    @foreach($responds as $respond)
                    <tr>
                        <th>{{$i++}}</th>
                        <td>
                            {{$respond->user->nim}}
                        </td>
                        <td>
                            {{$respond->user->name}}
                        </td>
                        <td>
                            <a href="{{route('respond.show',$respond->id)}}" class="button is-info">
                                <span class="icon">
                                    <i class="fas fa-eye" aria-hidden="true"></i>
                                </span>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $responds->links('vendor.pagination.bulma') }}
        </div>
    </div>
</div>
@endsection
