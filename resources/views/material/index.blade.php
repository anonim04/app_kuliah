@extends('layouts.app')

@section('content')
<div class="card">
    <header class="card-header">
        <p class="card-header-title">
            Modul
        </p>
        @if(Auth::user()->level=='admin')
        <span class="card-header-icon" aria-label="more options" data-target="modal-id">
            <button class="button modal-button is-success" data-toggle="modal"  data-target="modalInsertFile">
                <span class="icon">
                  <i class="fas fa-file-upload" aria-hidden="true"></i>
                </span>
            </button>
            <div id="modalInsertFile" class="modal modal-fx-3dSlit">
                <div class="modal-background"></div>
                    <div class="modal-content">
                        <header class="modal-card-head">
                            <div class="field">
                                <p class="modal-card-title">
                                    Upload File
                                </p>
                            </div>
                        </header>
                        <section class="modal-card-body">
                            {{ Form::open(['url'=>'/material/save', 'method' => 'post','files' => true]) }}
                                <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                        <label class="label">Pertemuan</label>
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <p class="control">
                                                @php
                                                    $meeting = '1';
                                                @endphp
                                                @foreach($materials as $key=>$value)
                                                    @php
                                                        $meeting++
                                                    @endphp
                                                @endforeach
                                                {!! Form::selectRange('pertemuan', $meeting,16); !!}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                @for($repeat = 0; $repeat<'2'; $repeat++)
                                    <div class="field is-horizontal">
                                        <div class="field-label is-normal">
                                            <label class="label">File</label>
                                        </div>
                                        <div class="field-body">
                                            <div class="field">
                                                <p class="control">
                                                    <div class="file has-name is-boxed">
                                                        <label class="file-label">
                                                            {{Form::file('file['.$repeat.']',['class'=>'file-input'])}}
                                                            <span class="file-cta" for="file">
                                                                <span class="file-icon">
                                                                    <i class="fas fa-upload"></i>
                                                                </span>
                                                                <span class="file-label">
                                                                    Choose a file…
                                                                </span>
                                                            </span>
                                                            <span class="file-name" >
                                                                -
                                                            </span>
                                                        </label>
                                                    </div>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                @endfor
                        </section>
                        <footer class="modal-card-foot buttons is-right">
                            <button type="button" class="button button-close-modal" aria-label="close">
                                Cancel
                            </button>
                            {{Form::submit('Upload',['class'=>'button is-success is-right',])}}
                        </footer>
                        {{Form::close()}}
                    </div>
                <button class="modal-close is-large" aria-label="close"></button>
            </div>
        </span>
        @endif
    </header>
    <div class="card-content">
        <div class="content">
            <table class="table is-fullwidth is-striped">
                <tbody>
                    @php
                      $i= ($pagination->currentpage() - 1) * $pagination->perpage() + 1;
                    @endphp
                    @foreach($materials as $key=>$material)
                    <tr>
                        <th>{{$i++}}</th>
                        <th>Pertemuan {{$material->pertemuan}}</th>
                        <td>
                            <div class="buttons is-right">
                                <div class="dropdown">
                                  <div class="dropdown-trigger">
                                      <button class="button is-info" aria-haspopup="true" aria-controls="dropdown-menu">
                                          <span class="icon">
                                              <i class="fas fa-file-download" aria-hidden="true"></i>
                                          </span>
                                      </button>
                                  </div>
                                  <div class="dropdown-menu" id="dropdown-menu" role="menu">
                                      <div class="dropdown-content">
                                          @foreach($material->materialFiles()->get() as $materialFile)
                                          @if($materialFile->type == 'pdf')
                                              <a href="{{url('material/download/modul/'.$materialFile->id)}}" class="dropdown-item">
                                                  Modul
                                              </a>
                                        @else
                                            <a href="{{url('material/download/ppt/'.$materialFile->id)}}" class="dropdown-item">
                                                PPT
                                            </a>
                                        @endif
                                          @endforeach
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="buttons is-centered">
                                <a href="{{url('quiz/'.$material->id)}}" class="button is-info" onclick="myStartFunction()">
                                    <span class="icon">
                                        <i class="fas fa-eye" aria-hidden="true"></i>
                                    </span>
                                </a>
                            </div>
                        </td>
                        @if(Auth::user()->level=='admin')
                        <td>
                            <div class="buttons">
                                <button class="button modal-button is-danger" data-toggle="modal"  data-target="modalDeleteFile{{$material->id}}">
                                    <span class="icon">
                                      <i class="fas fa-trash-alt" aria-hidden="true"></i>
                                    </span>
                                </button>
                            </div>
                            <div id="modalDeleteFile{{$materials[$key]->id}}" class="modal modal-fx-3dSlit">
                                <div class="modal-background"></div>
                                <div class="modal-content">
                                    <header class="modal-card-head">
                                        <div class="field">
                                            <p class="modal-card-title">
                                                Konfirmasi Hapus Data
                                            </p>
                                        </div>
                                    </header>
                                    <section class="modal-card-body">
                                        Apakah Anda Yakin ingin Menghapus Data Ini ?
                                    </section>
                                    <footer class="modal-card-foot buttons is-right">
                                        <button type="button" name="button" class="button">Cancel</button>
                                        {{ Form::open(['url'=>'/material/delete/'.$material->id, 'method' => 'delete']) }}
                                        <button type="submit" class="button is-danger">
                                            Konfirmasi
                                        </button>
                                        {{ Form::close() }}
                                    </footer>
                                </div>
                                <button class="modal-close is-large" aria-label="close"></button>
                            </div>
                        </td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $pagination->links('vendor.pagination.bulma') }}
        </div>
    </div>
</div>
@endsection
