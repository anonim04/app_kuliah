@if(Session::has('flash_message'))
    <div class="notification is-success">
        <button class="delete" id="button-delete"></button>
        {{ Session::get('flash_message') }}
    </div>
@endif
