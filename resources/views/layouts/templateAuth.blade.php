<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{ asset('img/logo.png') }}" type="image/x-icon" />
    <title>Log In</title>
    <script src="{{ asset('js/particles.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.5.3/css/bulma.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
  </head>
<body>
    <div id="app">
        <main>
            <div class="columns is-vcentered">
                <div id="particles-js" class="interactive-bg column is-8"></div>
                <div class="login column is-4 ">
                <section class="section">
                  <div class="has-text-centered">
                      <img class="login-logo" src="{{ asset('img/android-icon-144x144.png') }}">
                  </div>
                  @yield('content')
                </section>
              </div>
            </div>
        </main>
    </div>
</body>
</html>
