<nav class="navbar is-dark" role="navigation" aria-label="main navigation">
    <div class="navbar-brand">
        <a class="navbar-item" href="{{ url('/') }}">
            <img src="{{ asset('img/logo.png') }}" alt="Bulma: a modern CSS framework based on Flexbox" width="30" height="28">
            <b>CSSC</b>
        </a>
        <div class="navbar-burger burger" data-target="navbarExampleTransparentExample">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
        </div>
    </div>

        <div class="navbar-menu" id="navbarExampleTransparentExample">
            <!-- Left Side Of Navbar -->
            <div class="navbar-start">
                <a class="navbar-item" href="{{ url('/') }}">
                    Home
                </a>
                @if(Auth::user()->level=='admin')
                <a class="navbar-item" href="{{ route('user.index') }}">
                    User
                </a>
                @endif
            </div>

            <!-- Right Side Of Navbar -->
            <div class="navbar-end">
                @if(Auth::check())
                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link">
                        {{ Auth::user()->name }}
                    </a>

                    <div class="navbar-dropdown is-boxed">
                        <a class="navbar-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
                @else
                <a class="navbar-item" href="{{ url('login') }}">{{ __('Login') }}</a>
                @endif
            </div>
        </div>
    </nav>
