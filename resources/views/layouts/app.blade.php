<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="{{ asset('img/logo.png') }}" type="image/x-icon" />
        <title>App_Kuliah</title>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('css/bulma.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/bulma-extensions.min.css') }}" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.css" rel="stylesheet">
        <style media="screen">
            @import  url(https://fonts.googleapis.com/css?family=Quattrocento+Sans);
            .loading {
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background: rgba(0, 0, 0, 0.9);
                z-index: 9999;
            }

            .loading-text {
                position: absolute;
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;
                margin: auto;
                text-align: center;
                width: 100%;
                height: 100px;
                line-height: 100px;
            }
            .loading-text span {
                display: inline-block;
                margin: 0 5px;
                color: #fff;
                font-family: "Quattrocento Sans", sans-serif;
            }
            .loading-text span:nth-child(1) {
                filter: blur(0px);
                animation: blur-text 1.5s 0s infinite linear alternate;
            }
            .loading-text span:nth-child(2) {
                filter: blur(0px);
                animation: blur-text 1.5s 0.2s infinite linear alternate;
            }
            .loading-text span:nth-child(3) {
                filter: blur(0px);
                animation: blur-text 1.5s 0.4s infinite linear alternate;
            }
            .loading-text span:nth-child(4) {
                filter: blur(0px);
                animation: blur-text 1.5s 0.6s infinite linear alternate;
            }
            .loading-text span:nth-child(5) {
                filter: blur(0px);
                animation: blur-text 1.5s 0.8s infinite linear alternate;
            }
            .loading-text span:nth-child(6) {
                filter: blur(0px);
                animation: blur-text 1.5s 1s infinite linear alternate;
            }
            .loading-text span:nth-child(7) {
                filter: blur(0px);
                animation: blur-text 1.5s 1.2s infinite linear alternate;
            }

            @keyframes  blur-text {
                0% {
                    filter: blur(0px);
                }
                100% {
                    filter: blur(4px);
                }
            }
    </style>
    </head>
    <body>
        <div class="loading" id="loading">
            <div class="loading-text">
                <span class="loading-text-words">L</span>
                <span class="loading-text-words">O</span>
                <span class="loading-text-words">A</span>
                <span class="loading-text-words">D</span>
                <span class="loading-text-words">I</span>
                <span class="loading-text-words">N</span>
                <span class="loading-text-words">G</span>
            </div>
        </div>
        <main>
            @include('layouts.navbar')
                <section class="hero is-success is-bold">
                    <div class="hero-body">
                        <div class="container has-text-centered">
                            <h2 class="subtitle">
                                MATA KULIAH
                            </h2>
                            <h1 class="title">
                                PENGANTAR PEMROGRAMAN
                            </h1>
                        </div>
                    </div>
                </section>
                <div class="columns is-mobile">
                    <div class="column is-1"></div>
                    <div class="column">
                        <br>
                        @include('vendor.notifications.flashMessage')
                        @yield('content')
                    </div>
                    <div class="column is-1"></div>
                </div>
            <br>
            @include('layouts.footer')
        </main>

        <!-- Scripts -->
        <script defer src="https://use.fontawesome.com/releases/v5.1.0/js/all.js"></script>
        <script defer src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
        <script defer src="{{ asset('js/modal.js') }}"></script>
        <script defer src="{{ asset('js/file.js') }}"></script>
        <script src="{{ asset('js/bulma-steps.min.js') }}" charset="utf-8"></script>
        <script ></script>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.js"></script>
        <script> bulmaSteps.attach(); </script>

        <script type="text/javascript">
            $('.summernote').summernote({
                tabsize: 2,
                height: 400
            });
        </script>
        <script src="{{ asset('js/timer.js') }}"></script>
    </body>
</html>
