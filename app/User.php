<?php

namespace App;

use App\Respond;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'nim','name', 'email','level', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function groupstudents()
    {
        return $this->hasMany(GroupStudent::class, 'user_id');
    }
    public function respond()
    {
        return $this->hasMany(Respond::class);
    }
}
