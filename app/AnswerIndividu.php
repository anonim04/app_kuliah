<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnswerIndividu extends Model
{
    protected $fillable=[
        'respond_id',
        'answer',
        'picAnswer',
        'dirPic',
        'type',
    ];

    public function respond(){
        return $this->belongsTo(Respond::class);
    }
}
