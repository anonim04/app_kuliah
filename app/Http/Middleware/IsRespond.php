<?php

namespace App\Http\Middleware;

use Closure;
use App\GroupStudent;

class IsRespond
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $model = GroupStudent::all();
        dd($model);
        if ($model->leader==1) {
            return $next($request);
        }
        Session::flash('flash_message', 'Maaf, anda tidak berhak mengakses situs tersebut');
        return redirect(route('home'));
    }
}
