<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MaterialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file[0]' => 'mimes:pptx,pptm,ppt,pdf,xps,potx,potm,pot,thmx,ppsx,ppsm,pps,ppam,PPA,xml,PDF,bmp,wmf,emf,rtf,pptx,ODP',
            'file[1]' => 'mimes:doc,pdf,PDF,docx,zip,docm,dotx,dotm',
        ];
    }
}
