<?php

namespace App\Http\Controllers;

use DB;
use Storage;
use Session;
use App\User;
use App\Answer;
use App\Respond;
use App\AnswerGroup;
use App\GroupStudent;
use App\QuizQuestion;
use App\AnswerIndividu;
use Illuminate\Http\Request;
use App\Http\Requests\QuizRequest;
use App\Http\Requests\RespondRequest;
use Intervention\Image\ImageManagerStatic as Image;

class RespondController extends Controller
{
    public function index($quiz_id)
    {
        $responds = Respond::where('quiz_id',$quiz_id)->paginate(10);
        return view('respond.index', compact('responds'));
    }

    public function group($quiz_id)
    {
        $responds = Respond::where('quiz_id',$quiz_id)->paginate(10);
        $groups = GroupStudent::all();
        return view('respond.group', compact('responds','groups'));
    }


    public function store(Request $request)
    {
        $modelrespond = new Respond();
        $modelrespond->user_id = $request->get('user_id');
        $modelrespond->quiz_id = $request->get('quiz_id');
        if($modelrespond->save()){
            foreach ($request->answer as $key => $value) {
                if($modelrespond->quiz->answer=='individu')
                {
                    $model = new AnswerIndividu();
                }
                else if($modelrespond->quiz->answer=='kelompok'){
                    $model = new AnswerGroup();
                }
                $model->respond_id = $modelrespond->id;
                $model->answer = $request->answer[$key];
                if($request->hasFile('picAnswer.'.$key)){
                    $fileName = $request->picAnswer[$key]->getClientOriginalName();
                    $model->picAnswer = $fileName;
                    $model->dirPic = "/storage/app/picture/answer";
                    $model->type = $request->picAnswer[$key]->getClientOriginalExtension();
                    $request->picAnswer[$key]->storeAs('picture/answer', $fileName);
                }
                $model->save();
            }
        }
        Session::flash('flash_message', 'Pengisian Jawaban telah selesai');
        return redirect('/home');
    }

    public function show($respond_id){
        $respond = Respond::findOrFail($respond_id);
        if($respond->quiz->answer=='individu')
        {
            $answer = $respond->answerindividus()->get();
        }
        else if($respond->quiz->answer=='kelompok')
        {
            $answer = $respond->answergroup()->get();
        }
        $quiz = QuizQuestion::all();
        $question = $quiz->where('quiz_id',$respond->quiz_id);
        return view('respond.show', compact('answer','respond','question'));
    }
}
