<?php

namespace App\Http\Controllers;

use DB;
use File;
use Store;
Use Session;
use App\Material;
use App\MaterialFile;
use Illuminate\Http\Request;
use App\Http\Requests\MaterialRequest;

class MaterialController extends Controller
{
    public function index()
    {
        $materials = Material::all();
        $pagination = Material::orderBy('id','asc')->paginate(8);
        return view('material.index', compact('materials', 'pagination'));
    }

    public function storeFile(MaterialRequest $request)
    {
        $modelMaterial = new Material();
        $modelMaterial->pertemuan = $request->get('pertemuan');
        if($modelMaterial->save()){
            foreach ($request->file as $key => $value) {
                $dir = '';
                $modelMaterial_files = new MaterialFile();
                $modelMaterial_files->material_id = $modelMaterial->id;
                $modelMaterial_files->fileName = $request->file[$key]->getClientOriginalName();
                if($key==0){
                    $modelMaterial_files->dir = 'storage/app/document/modul';
                    $dir = 'modul';
                }
                else if($key==1) {
                    $modelMaterial_files->dir = 'storage/app/document/ppt';
                    $dir = 'ppt';
                }
                $modelMaterial_files->type = $request->file[$key]->getClientOriginalExtension();
                $request->file[$key]->storeAs('document/'.$dir, $modelMaterial_files->fileName);
                $modelMaterial_files->save();
            }
        }
        Session::flash('flash_message', 'File Berhasil di Upload');
        return redirect('/home');
    }

    public function getFile($id){
        $model = MaterialFile::findOrFail($id);
        $dirfile = $model->dir;
        $file = '../'.$dirfile.'/'.$model->fileName;
        $name = basename($file);
        return response()->download($file, $name);
    }

    public function destroy($id)
    {
        $file = Material::findOrFail($id);
        $file->delete();
        Session::flash('flash_message', 'File Berhasil Dihapus');
        return redirect('/home');
    }
}
