<?php

namespace App\Http\Controllers;

use Session;
use App\Quiz;
use App\User;
use App\QuizQuestion;
use App\GroupStudent;
use Illuminate\Http\Request;
use App\Http\Requests\QuizRequest;


class QuizController extends Controller
{
    public function index($material_id)
    {
        $question = Quiz::where('material_id',$material_id)->paginate(10);
        $group = GroupStudent::where('material_id', $material_id)->get();
        return view('quiz.index', compact('question', 'group','material_id'));
    }

    public function store(Request $request)
    {

        $modelQuiz = new Quiz();
        $modelQuiz->material_id = $request->get('material_id');
        $modelQuiz->title = $request->get('title');
        $modelQuiz->answer = $request->get('answer');
        $modelQuiz->save();
        foreach ($request->question as $key => $value)
        {
            $modelQuizQuestion = new QuizQuestion();
            $modelQuizQuestion->quiz_id = $modelQuiz->id;
            $modelQuizQuestion->material_id = $request->get('material_id');
            $modelQuizQuestion->question = $request->question[$key];
            $modelQuizQuestion->save();
        }
        Session::flash('flash_message', 'Quiz berhasil Dibuat');
        return redirect(route('quiz.index',$modelQuiz->material_id));
    }

    public function destroy($id)
    {
        $file = Quiz::findOrFail($id);
        $file->delete();
        Session::flash('flash_message', 'Data Quiz Berhasil Dihapus');
        return redirect('home');
    }

    public function showIndividu($quiz_id)
    {
        $model = Quiz::findOrFail($quiz_id);
        $meeting = $model->material_id;
        $model = QuizQuestion::all();
        $quiz = $model->where('quiz_id',$quiz_id);
        $question = $quiz->where('material_id',$meeting);
        return view('quiz.show', compact('question','quiz_id'));
    }

    public function showGroup($quiz_id)
    {
        $model = Quiz::findOrFail($quiz_id);
        $meeting = $model->material_id;
        $model = QuizQuestion::all();
        $quiz = $model->where('quiz_id',$quiz_id);
        $question = $quiz->where('material_id',$meeting);
        return view('quiz.group', compact('question','quiz_id'));
    }
}
