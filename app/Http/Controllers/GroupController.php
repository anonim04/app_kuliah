<?php

namespace App\Http\Controllers;

use App\User;
use App\GroupStudent;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function create($material_id)
    {
        $users = User::all();
        $pagination = User::paginate(10);
        return view('group.create',compact('users','pagination','material_id'));
    }

    public function store(Request $request, $material_id)
    {
        foreach ($request->user_id as $key => $value) {
            if (GroupStudent::where('user_id', $request->user_id[$key])->count()) {
                $groupStudent = GroupStudent::where('user_id', $request->user_id[$key])->first();
            }
            else {
                $groupStudent = new GroupStudent();
            }
            $groupStudent->nameGroup =  $request->nameGroup[$key];
            $groupStudent->user_id = $request->user_id[$key];
            $groupStudent->material_id = $material_id;
            if($request->level[$key]==1)
            {
                $groupStudent->leader = 1;
                $groupStudent->member = 0;
            }
            else if($request->level[$key]==0)
            {
                $groupStudent->leader = 0;
                $groupStudent->member = 1;
            }
            $groupStudent->save();
        }
        return redirect('home');
    }
}
