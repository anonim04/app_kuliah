<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    protected $fillable =[
        'pertemuan',
        'nameFile',
        'dir',
        'type',
    ];

    public function quiz(){
        return $this->hasMany(Quiz::class, 'material_id');
    }
    public function materialFiles()
    {
        return $this->hasMany(MaterialFile::class, 'material_id');
    }
}
