<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Respond extends Model
{

    protected $fillable=[
        'user_id',
        'quiz_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function quiz()
    {
        return $this->belongsTo(Quiz::class);
    }
    public function answerindividus()
    {
        return $this->hasMany(AnswerIndividu::class);
    }
    public function answergroup()
    {
        return $this->hasMany(AnswerGroup::class);
    }
}
