<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupStudent extends Model
{
    protected $fillable =[
        'nameGroup',
        'user_id',
        'material_id',
        'leader',
        'member'
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
