<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    protected $fillable =[
        'quiz_id',
        'material_id',
        'question',
    ];

    public function material()
    {
        return $this->belongsTo(Material::class, 'material_id');
    }

    public function respond()
    {
        return $this->hasMany(Respond::class);
    }

    public function quizQuestion()
    {
        return $this->hasMany(QuizQuestion::class, 'quiz_id');
    }
}
