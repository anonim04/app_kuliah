<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaterialFile extends Model
{
    public function material()
    {
        return $this->belongsTo(Material::class, 'material_id');
    }
}
